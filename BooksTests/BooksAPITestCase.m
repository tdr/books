//
//  BooksAPITestCase.m
//  Books
//
//  Created by Pavel Yakimenko on 2/3/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "BooksAPI.h"

@interface BooksAPITestCase : XCTestCase

@end

@implementation BooksAPITestCase

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

- (void)testRequestBooksList
{
//  [self measureBlock:^{
    [BooksAPI requestBooksWithCompletion:^(NSArray *books, NSError *error) {
      XCTAssertNil(error, @"Error should be nil");
      XCTAssert(books.count, @"Ok");
    }];
//  }];
}

@end
