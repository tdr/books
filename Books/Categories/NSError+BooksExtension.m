//
//  NSError+BooksExtension.m
//  Books
//
//  Created by Pavel Yakimenko on 2/2/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import "NSError+BooksExtension.h"
#import "Constants.h"

@implementation NSError (BooksExtension)

+ (NSString *)booksDomain
{
  return @"Books domain";
}

+ (NSError *)booksNotImplemented
{
  return [NSError errorWithDomain:[self booksDomain] code:BE_NotImplemented userInfo:@{NSLocalizedDescriptionKey: @"Not implemented"}];
}

+ (NSError *)usernameNotProvided
{
  return [NSError errorWithDomain:[self booksDomain] code:BE_UsernameNotProvided userInfo:@{NSLocalizedDescriptionKey: @"Please provide username"}];
}

+ (NSError *)commentIsTooShort
{
  NSString *errorMessage = [NSString stringWithFormat:@"Comment is too short. Must be more than %zd symbols", [Constants minimumCommentLength]];
  return [NSError errorWithDomain:[self booksDomain] code:BE_CommentTextIsTooShort userInfo:@{NSLocalizedDescriptionKey: errorMessage}];
}

+ (NSError *)bookNotProvided
{
  return [NSError errorWithDomain:[self booksDomain] code:BE_BookNotProvided userInfo:@{NSLocalizedDescriptionKey: @"Provide book"}];
}

+ (NSError *)badResponse
{
  return [NSError errorWithDomain:[self booksDomain] code:BE_BadResponse userInfo:@{NSLocalizedDescriptionKey: @"Bad response format"}];
}

@end
