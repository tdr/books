//
//  NSError+BooksExtension.h
//  Books
//
//  Created by Pavel Yakimenko on 2/2/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
  BE_OK,
  BE_NotImplemented,
  BE_UsernameNotProvided,
  BE_CommentTextIsTooShort,
  BE_BookNotProvided,
  BE_BadResponse,
} BooksErrorCode;

@interface NSError (BooksExtension)

+ (NSError *)booksNotImplemented;
+ (NSError *)usernameNotProvided;
+ (NSError *)commentIsTooShort;
+ (NSError *)bookNotProvided;
+ (NSError *)badResponse;

@end
