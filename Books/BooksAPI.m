//
//  BooksAPI.m
//  Books
//
//  Created by Pavel Yakimenko on 2/2/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import "BooksAPI.h"
#import "NSError+BooksExtension.h"
#import "Constants.h"
#import "Book.h"

static NSString * const kBooksListRequest = @"http://54.152.155.201/books.json";
static NSString * const kPostFeedbackRequest = @"http://54.152.155.201/book/%zd";
static NSString * const kJSONFeedbackBody = @"{\"name\":\"%@\",\"comment\":\"%@\"}";

@implementation BooksAPI

+ (void)requestBooksWithCompletion:(BooksRequestCompletionBlock)completionBlock
{
  if(!completionBlock)
  {
    return;
  }
  
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    NSError *error = nil;
    NSMutableArray *booksArray = nil;
    NSData *response = [self getDataWithRequest:kBooksListRequest error:&error];
    if(error)
    {
      NSLog(@"request error '%@ - %@'", error.localizedDescription, error.localizedFailureReason);
    }
    else
    {
      NSArray *booksRawData = [NSJSONSerialization JSONObjectWithData:response options:0 error:&error];
      NSLog(@"response: '%@'", booksRawData);
      if(!error)
      {
        if(![booksRawData isKindOfClass:[NSArray class]])
        {
          error = [NSError badResponse];
        }
        else
        {
          booksArray = [NSMutableArray array];
          for(NSDictionary *bookDictionary in booksRawData)
          {
            Book *book = [Book bookWithDictionary:bookDictionary];
            [booksArray addObject:book];
          }
        }
      }
    }

    completionBlock(booksArray, error);
  });
}

+ (void)postFeedback:(NSString *)comment book:(Book *)book onBehalf:(NSString *)username completion:(CommentPostCompletionBlock)completionBlock
{
  if(!completionBlock)
  {
    return;
  }
  if(username.length == 0)
  {
    completionBlock([NSError usernameNotProvided]);
  }
  else if(comment.length < [Constants minimumCommentLength])
  {
    completionBlock([NSError commentIsTooShort]);
  }
  else if(!book)
  {
    completionBlock([NSError bookNotProvided]);
  }

  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    NSError *error = nil;
    NSMutableArray *booksArray = nil;
    NSString *requestString = [NSString stringWithFormat:kPostFeedbackRequest, book.bookId];
    NSString *bodyString = [NSString stringWithFormat:kJSONFeedbackBody, username, comment];
    NSData *response = [self postDataWithRequest:requestString jsonBody:bodyString error:&error];
    if(error)
    {
      NSLog(@"request error '%@ - %@'", error.localizedDescription, error.localizedFailureReason);
    }
    else
    {
      NSDictionary *responseRawData = [NSJSONSerialization JSONObjectWithData:response options:0 error:&error];
      NSLog(@"response: '%@'", responseRawData);
      if(!error)
      {
        if(![responseRawData isKindOfClass:[NSDictionary class]])
        {
          error = [NSError badResponse];
        }
        else
        {
          booksArray = [NSMutableArray array];
          NSString *status = responseRawData[@"status"];
          NSInteger bookId = [responseRawData[@"id"] integerValue];
          if([status isEqualToString:@"ok"] &&
             bookId == book.bookId)
          {
            error = nil;
          }
          else
          {
            error = [NSError badResponse];
          }
        }
      }
    }
    
    completionBlock(error);
  });
}

#pragma mark - Private

+ (NSData *)getDataWithRequest:(NSString *)requestString error:(NSError **)error
{
  NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:requestString]];
  NSURLResponse *requestResponse = nil;
  NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:error];
  return responseData;
}

+ (NSData *)postDataWithRequest:(NSString *)requestString jsonBody:(NSString *)jsonString error:(NSError **)error
{
  NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString]];
  request.HTTPMethod = @"POST";
  [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  request.HTTPBody = [jsonString dataUsingEncoding:NSUTF8StringEncoding];

  NSURLResponse *requestResponse = nil;
  NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:error];
  return responseData;
}

@end
