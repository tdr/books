//
//  BookShelfViewController.h
//  Books
//
//  Created by Pavel Yakimenko on 2/3/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookShelfViewController : UITableViewController

@end
