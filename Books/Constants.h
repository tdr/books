//
//  Constants.h
//  Books
//
//  Created by Pavel Yakimenko on 2/3/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kBookIdKey;
extern NSString * const kBookTitleKey;
extern NSString * const kBookAuthorKey;
extern NSString * const kBookCoverUrlKey;

@interface Constants : NSObject

+ (NSUInteger)minimumCommentLength;

@end
