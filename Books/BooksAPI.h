//
//  BooksAPI.h
//  Books
//
//  Created by Pavel Yakimenko on 2/2/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Book;

typedef void(^BooksRequestCompletionBlock)(NSArray *books, NSError *error);
typedef void(^CommentPostCompletionBlock)(NSError *error);

@interface BooksAPI : NSObject

+ (void)requestBooksWithCompletion:(BooksRequestCompletionBlock)completionBlock;
+ (void)postFeedback:(NSString *)comment book:(Book *)book onBehalf:(NSString *)username completion:(CommentPostCompletionBlock)completionBlock;

@end
