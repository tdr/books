//
//  Constants.m
//  Books
//
//  Created by Pavel Yakimenko on 2/3/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import "Constants.h"

NSString * const kBookIdKey = @"id";
NSString * const kBookTitleKey = @"title";
NSString * const kBookAuthorKey = @"author";
NSString * const kBookCoverUrlKey = @"cover_image";

@implementation Constants

+ (NSUInteger)minimumCommentLength
{
  return 100;
}

@end
