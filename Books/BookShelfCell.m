//
//  BookShelfCell.m
//  Books
//
//  Created by Pavel Yakimenko on 2/4/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import "BookShelfCell.h"
#import "Book.h"

@implementation BookShelfCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureWithBook:(Book *)book
{
  self.textLabel.text = book.author;
  self.detailTextLabel.text = book.title;
}

@end
