//
//  BookShelfCell.h
//  Books
//
//  Created by Pavel Yakimenko on 2/4/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Book;

@interface BookShelfCell : UITableViewCell

- (void)configureWithBook:(Book *)book;

@end
