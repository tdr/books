//
//  Book.m
//  Books
//
//  Created by Pavel Yakimenko on 2/3/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import "Book.h"
#import "Constants.h"

@implementation Book

+ (instancetype)bookWithDictionary:(NSDictionary *)bookDictionary
{
  Book *newBook = [[Book alloc] init];
  if(newBook)
  {
    newBook.author = bookDictionary[kBookAuthorKey];
    newBook.coverImageURL = [NSURL URLWithString:bookDictionary[kBookCoverUrlKey]];
    newBook.bookId = [bookDictionary[kBookIdKey] unsignedIntegerValue];
    newBook.title = bookDictionary[kBookTitleKey];
  }
  return newBook;
}

@end
