//
//  BookFeedback.h
//  Books
//
//  Created by Pavel Yakimenko on 2/4/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class Book;

@interface BookFeedback : NSObject

- (instancetype)initWithBook:(Book *)book;

- (void)showFeedbackForViewController:(UIViewController *)viewController;

@end
