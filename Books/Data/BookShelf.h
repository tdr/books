//
//  BookShelf.h
//  Books
//
//  Created by Pavel Yakimenko on 2/4/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Book, BookShelf;

@protocol BookShelfDelegate <NSObject>

@optional
- (void)bookShelfWillReload:(BookShelf *)bookShelf;
- (void)bookShelfDidReload:(BookShelf *)bookShelf withError:(NSError *)error;

@end

@interface BookShelf : NSObject

@property (nonatomic, weak) id<BookShelfDelegate> delegate;

- (instancetype)initWithDelegate:(id<BookShelfDelegate>)delegate;

- (void)reload;
- (NSInteger)countOfBooks;
- (Book *)bookAtIndex:(NSInteger)index;

@end
