//
//  BookFeedback.m
//  Books
//
//  Created by Pavel Yakimenko on 2/4/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import "BookFeedback.h"
#import "Book.h"
#import "BooksAPI.h"
#import "Constants.h"

@interface BookFeedback () <UITextFieldDelegate>

@property (nonatomic, strong) Book *book;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *feedback;

@property (nonatomic, strong) UIAlertController *feedbackAlert;
@property (nonatomic, weak) UITextField *nameTextField;
@property (nonatomic, weak) UITextField *messageTextField;
@property (nonatomic, weak) UITextField *counterTextField;
@property (nonatomic, weak) UIAlertAction *sendAction;
@end

@implementation BookFeedback

- (instancetype)initWithBook:(Book *)book
{
  if(self = [super init])
  {
    self.book = book;
  }
  return self;
}

- (void)showFeedbackForViewController:(UIViewController *)viewController
{
  if(self.feedbackAlert)
  {
    [self.feedbackAlert dismissViewControllerAnimated:YES completion:nil];
    self.feedbackAlert = nil;
  }
  NSString *title = @"Feedback";
  NSString *message = [NSString stringWithFormat:@"on book '%@' by %@", self.book.title, self.book.author];

  self.feedbackAlert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
  
  self.sendAction = [UIAlertAction actionWithTitle:@"Send" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    self.username = self.nameTextField.text;
    self.feedback = self.messageTextField.text;
    self.feedbackAlert = nil;
    [self sendFeedback];
  }];
  self.sendAction.enabled = NO;
  [self.feedbackAlert addAction:self.sendAction];
  
  UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    self.username = nil;
    self.feedback = nil;
    self.feedbackAlert = nil;
  }];
  [self.feedbackAlert addAction:cancel];

  __block BookFeedback *weakSelf = self;
  
  [self.feedbackAlert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
    textField.placeholder = @"User name";
    textField.delegate = weakSelf;
    weakSelf.nameTextField = textField;
  }];
  
  [self.feedbackAlert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
    textField.placeholder = @"Feedbak text";
    textField.delegate = weakSelf;
    weakSelf.messageTextField = textField;
  }];
  
  [self.feedbackAlert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
    textField.placeholder = [NSString stringWithFormat:@"Need more symbols (%zd)", [Constants minimumCommentLength]];
    textField.delegate = weakSelf;
    textField.enabled = NO;
    weakSelf.counterTextField = textField;
  }];
  
  [viewController presentViewController:self.feedbackAlert animated:YES completion:nil];
  
  self.nameTextField.text = @"Test username";
  self.messageTextField.text = @"Comment with length long enough to fit in checking rules and allow me to send this comment to server.";
  [self textField:self.messageTextField shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:@""];
}

- (void)sendFeedback
{
  [BooksAPI postFeedback:self.feedback book:self.book onBehalf:self.username completion:^(NSError *error) {
    NSLog(@"post error %@", error);
    dispatch_async(dispatch_get_main_queue(), ^{
      if(error)
      {
        [[[UIAlertView alloc] initWithTitle:@"Send Feedback Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
      }
      else
      {
        [[[UIAlertView alloc] initWithTitle:@"Send Feedback" message:@"Your comment was successfully sent" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
      }
    });
  }];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
  if(textField == self.messageTextField)
  {
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if(resultString.length >= [Constants minimumCommentLength])
    {
      self.counterTextField.placeholder = @"Ready to send";
    }
    else
    {
      self.counterTextField.placeholder = [NSString stringWithFormat:@"Need more symbols (%zd)", [Constants minimumCommentLength] - resultString.length];
    }
  }

  self.sendAction.enabled = (self.messageTextField.text.length >= [Constants minimumCommentLength] &&
                             self.nameTextField.text.length);
  return YES;
}

@end
