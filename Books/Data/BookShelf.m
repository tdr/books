//
//  BookShelf.m
//  Books
//
//  Created by Pavel Yakimenko on 2/4/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import "BookShelf.h"
#import "BooksAPI.h"
#import "Book.h"

@interface BookShelf ()

@property (nonatomic, strong) NSArray *books;

@end

@implementation BookShelf

- (instancetype)initWithDelegate:(id<BookShelfDelegate>)delegate
{
  if(self = [super init])
  {
    self.delegate = delegate;
    [self reload];
  }
  return self;
}

- (void)reload
{
  if([self.delegate respondsToSelector:@selector(bookShelfWillReload:)])
  {
    [self.delegate bookShelfWillReload:self];
  }
  [BooksAPI requestBooksWithCompletion:^(NSArray *books, NSError *error) {
    if(!error)
    {
      self.books = books;
    }
    if([self.delegate respondsToSelector:@selector(bookShelfDidReload:withError:)])
    {
      [self.delegate bookShelfDidReload:self withError:error];
    }
  }];
}

- (NSInteger)countOfBooks
{
  return self.books.count;
}

- (Book *)bookAtIndex:(NSInteger)index
{
  NSAssert(index < self.books.count && index >= 0, @"Books array out of bound");
  return self.books[index];
}

@end
