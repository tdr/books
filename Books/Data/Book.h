//
//  Book.h
//  Books
//
//  Created by Pavel Yakimenko on 2/3/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Book : NSObject

@property (nonatomic, strong) NSString *author;
@property (nonatomic, strong) NSURL *coverImageURL;
@property (nonatomic, assign) NSUInteger bookId;
@property (nonatomic, strong) NSString *title;

+ (instancetype)bookWithDictionary:(NSDictionary *)bookDictionary;

@end
