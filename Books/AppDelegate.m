//
//  AppDelegate.m
//  Books
//
//  Created by Pavel Yakimenko on 2/2/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  // Override point for customization after application launch.
  return YES;
}

@end
