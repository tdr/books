//
//  BookShelfViewController.m
//  Books
//
//  Created by Pavel Yakimenko on 2/3/15.
//  Copyright (c) 2015 Pavel Yakimenko. All rights reserved.
//

#import "BookShelfViewController.h"
#import "BookShelf.h"
#import "Book.h"
#import "BookShelfCell.h"
#import "BookFeedback.h"

static NSString * const kBookTableViewCell = @"BookShelfCell";

@interface BookShelfViewController () <BookShelfDelegate>

@property (nonatomic, strong) BookShelf *shelf;

@end

@implementation BookShelfViewController

- (void)viewDidLoad {
  [super viewDidLoad];

  self.shelf = [[BookShelf alloc] initWithDelegate:self];
  
  [self setupRefreshControl];
  // Uncomment the following line to preserve selection between presentations.
  // self.clearsSelectionOnViewWillAppear = NO;
  
  // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
  // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  NSUInteger sectionCount = 0;
  if(self.shelf.countOfBooks)
  {
    self.tableView.backgroundView = nil;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    sectionCount = 1;
  }
  else
  {
    // Display a message when the table is empty
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    
    messageLabel.text = @"No data is currently available. Please pull down to refresh.";
    messageLabel.textColor = [UIColor blackColor];
    messageLabel.numberOfLines = 0;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    [messageLabel sizeToFit];
    
    self.tableView.backgroundView = messageLabel;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    sectionCount = 0;
  }
  
  return sectionCount;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return self.shelf.countOfBooks;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  BookShelfCell *cell = [tableView dequeueReusableCellWithIdentifier:kBookTableViewCell forIndexPath:indexPath];
  
  Book *book = [self.shelf bookAtIndex:indexPath.row];
  [cell configureWithBook:book];
  
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  Book *book = [self.shelf bookAtIndex:indexPath.row];
  BookFeedback *feedback = [[BookFeedback alloc] initWithBook:book];
  [feedback showFeedbackForViewController:self];
}

#pragma mark - Private

- (void)setupRefreshControl
{
  self.refreshControl = [[UIRefreshControl alloc] init];
  self.refreshControl.backgroundColor = [UIColor lightGrayColor];
  self.refreshControl.tintColor = [UIColor whiteColor];
  [self.refreshControl addTarget:self action:@selector(reloadTableData) forControlEvents:UIControlEventValueChanged];
}

- (void)reloadTableData
{
  [self.shelf reload];
}

- (void)finishReloadTableView
{
  if(self.refreshControl)
  {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm a"];
    NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
    NSDictionary *attrsDictionary = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
    self.refreshControl.attributedTitle = attributedTitle;
    
    [self.refreshControl endRefreshing];
  }
}

#pragma mark - BookShelfDelegate

- (void)bookShelfWillReload:(BookShelf *)bookShelf
{
  
}

- (void)bookShelfDidReload:(BookShelf *)bookShelf withError:(NSError *)error
{
  dispatch_async(dispatch_get_main_queue(), ^{
    [self.tableView reloadData];
    [self finishReloadTableView];
  });
}

@end
